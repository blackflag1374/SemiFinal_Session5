package com.example.admin.myapplication.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.myapplication.R;
import com.example.admin.myapplication.weather.Models.Condition;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Admin on 2/10/2018.
 */

public class ConditionListAdapter extends BaseAdapter {

    Context mContext ;
    List<Condition> condition;

    public ConditionListAdapter(Context mContext, List<com.example.admin.myapplication.weather.Models.Condition> condition) {
        this.mContext = mContext;
        this.condition = condition;

    }

    @Override
    public int getCount() {
        return condition.size();
    }

    @Override
    public Object getItem(int i) {
        return condition.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
     }

    @Override
    public View getView(int i , View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.condition, null);
        ImageView imgO = v.findViewById(R.id.imgO);
        TextView text = v.findViewById(R.id.text);

        Condition thatone = condition.get(i) ;

        text.setText(" Text : "+thatone.getText());



        if ( thatone.getText().toLowerCase().contains("sun"))
            Picasso.with(mContext).load(R.drawable.sunny).into(imgO);
        if (thatone.getText().toLowerCase().contains("cloud"))
            Picasso.with(mContext).load(R.drawable.cloudy).into(imgO);




        return v;

    }
}
