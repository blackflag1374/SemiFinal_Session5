package com.example.admin.myapplication.weather;

import android.content.Context;
import android.graphics.ImageFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.admin.myapplication.R;
import com.example.admin.myapplication.weather.Models.Forecast;
import com.example.admin.myapplication.weather.Models.Results;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Admin on 2/5/2018.
 */

public class ForecastListtAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public ForecastListtAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int i) {
        return forecasts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.forecastlist_item, null);

        TextView day = v.findViewById(R.id.day);
        TextView date = v.findViewById(R.id.date);
        TextView high = v.findViewById(R.id.high);
        TextView low = v.findViewById(R.id.low);
        TextView text = v.findViewById(R.id.text);
        ImageView img = v.findViewById(R.id.img);


        Forecast thisone = forecasts.get(i);
        day.setText(" Day : "+thisone.getDay() );
        date.setText(" Date : " +thisone.getDate());
        high.setText(" High : " +   PublicMethod.convertFtoC( thisone.getHigh()));
        low.setText(" Low : "+  PublicMethod.convertFtoC(thisone.getLow()));
        text.setText(" Text : "+thisone.getText());


        if ( thisone.getText().toLowerCase().contains("sun"))
            Picasso.with(mContext).load(R.drawable.sunny).into(img);
        if (thisone.getText().toLowerCase().contains("cloud"))
            Picasso.with(mContext).load(R.drawable.cloudy).into(img);



        return v;

    }
}
