package com.example.admin.myapplication.weather.MyTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Admin on 2/9/2018.
 */

public class MyTextView extends AppCompatTextView {
    private  Context context;

    public MyTextView(Context context) {
        super(context);
        this.context = context;
        setTypeface();
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setTypeface();

    }


    void setTypeface(){
        Typeface saf = Typeface.createFromAsset(context.getAssets(), "en_saf.ttf");
        this.setTypeface(saf);
    }
    
    
}
