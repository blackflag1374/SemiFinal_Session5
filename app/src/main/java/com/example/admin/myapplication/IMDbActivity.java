package com.example.admin.myapplication;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class IMDbActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView poster;
    TextView title, director, plot, released, Genre;
    EditText word;
    Button show;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imdb);
        progressDialog = new ProgressDialog(this);
        bandied();
        show.setOnClickListener(this);

    }

    void bandied() {
        poster = findViewById(R.id.poster);
        Genre = findViewById(R.id.genre);
        title = findViewById(R.id.title);
        released = findViewById(R.id.released);
        director = findViewById(R.id.director);
        plot = findViewById(R.id.plot);
        word = findViewById(R.id.word);
        show = findViewById(R.id.show);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("Please wait until respone load");
    }

    @Override
    public void onClick(View view) {
        getdatafromOMDb(word.getText().toString());
    }

    void getdatafromOMDb(String word) {
        progressDialog.show();
        String url = "http://www.omdbapi.com/?t=" + word + "&apikey=70ad462a";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(IMDbActivity.this, "error in connection", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                getdataformJsonString(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }
        });

    }

    void getdataformJsonString(String serverResponse) {




        try {
            JSONObject allobject = new JSONObject(serverResponse);
            String titlevalue = allobject.getString("Title");
            String releasedvalue = allobject.getString("Released");
            String genrevalue = allobject.getString("Genre");
            String Directorvalue = allobject.getString("Director");
            String plotvalue = allobject.getString("Plot");
            String postervalue = allobject.getString("Poster");


            title.setText( "Title: " + titlevalue);
            director.setText( "Director: " + Directorvalue);
            released.setText("Released: " + releasedvalue);
            Genre.setText("Genre: " + genrevalue);
            plot.setText("Plot: " + plotvalue);
            Picasso.with(this).load(postervalue).into(poster);

        } catch (JSONException e) {
            Toast.makeText(this, "data has been failed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }
}
