package com.example.admin.myapplication.weather;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.admin.myapplication.R;
import com.example.admin.myapplication.weather.Models.Condition;
import com.example.admin.myapplication.weather.Models.Forecast;
import com.example.admin.myapplication.weather.Models.WeatherModel;
import com.example.admin.myapplication.weather.Models.YahooModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class WeatherActivity extends AppCompatActivity implements View.OnClickListener {
    EditText city;
    TextView result , text , txt ;
    ProgressDialog dialog;
    ListView forecastList;
    ImageView icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        city = findViewById(R.id.city);
        result = findViewById(R.id.result);
        text = findViewById(R.id.text);
        txt = findViewById(R.id.txt);
        forecastList = findViewById(R.id.forecastList);
        dialog = new ProgressDialog(this);
        findViewById(R.id.show).setOnClickListener(this);
        dialog.setTitle("Loading");
        dialog.setMessage("Please wait until response laod");
        Typeface irSans = Typeface.SANS_SERIF.createFromAsset(getAssets(), "ir_sans.ttf");
        result.setTypeface(irSans);
        icon = findViewById(R.id.icon);




    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show) {
            String resultvalue = result.getText().toString();
            String cityvalue = city.getText().toString();
            WeatherDBHandler db = new WeatherDBHandler( this ,"seamtec.db",null , 1  );
            db.insertWeather(cityvalue , resultvalue);
            Toast.makeText(this, " new city has been saved", Toast.LENGTH_SHORT).show();
            getDataFromYahoo(cityvalue);

        }
    }
    private void getDataFromYahoo(String cityvalue) {
        dialog.show();
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                cityvalue + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(WeatherActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseData(responseString);
            }

            @Override
            public void onFinish() {
                super.onFinish();
                dialog.dismiss();
            }
        });
    }

    private void parseData(String responseString) {
        Gson gson = new Gson();
        WeatherModel yahoo = gson.fromJson(responseString, WeatherModel.class);
        String resultStr = yahoo.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        result.setText(city.getText().toString() + " : " + resultStr);

        Gson ggson = new Gson() ;
        WeatherModel yahooo = ggson.fromJson(responseString,WeatherModel.class);
        String resultStra = yahooo.getQuery().getResults().getChannel().getItem().getCondition().getText();
        result.setText(txt.getText().toString() + " : " + resultStra);

        txt.setText(yahooo.getQuery().getResults().getChannel().getItem().getCondition().getText());
        if(resultStra.contains("sun"));
        Picasso.with(this).load(R.drawable.sunny).into(icon);
        if(resultStra.contains("cloud"));
        Picasso.with(this).load(R.drawable.cloudy).into(icon);


        result.setText(city.getText().toString() + " : " + PublicMethod.convertFtoC(resultStr));
        //forecast list
        ForecastListtAdapter adapter = new ForecastListtAdapter(this, yahoo.getQuery().getResults().getChannel().getItem().getForecast());
        forecastList.setAdapter(adapter);
    }


}
