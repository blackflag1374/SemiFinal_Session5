package com.example.admin.myapplication.weather;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Admin on 2/9/2018.
 */

public class WeatherDBHandler extends SQLiteOpenHelper {
    String tblCreate = "" +
            "CREATE TABLE weather ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT , " +
            "result TEXT ," +
            "city TEXT , " +
            "_)";


    public WeatherDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    public void insertWeather(String result, String city) {
        String insertQuery = "" +
                " INSERT INTO weather (result , city) " +
                "VALUES ('" +  city + "' , '" + result + "'  )";
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();


    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(tblCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {


    }
}
